package by.zapotylok.task1.exception;

public class FileMistakeException extends Exception {

	private static final long serialVersionUID = 798178093620656352L;

	public FileMistakeException() {
		super();
	}

	public FileMistakeException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileMistakeException(String message) {
		super(message);
	}

	public FileMistakeException(Throwable cause) {
		super(cause);
	}

}
